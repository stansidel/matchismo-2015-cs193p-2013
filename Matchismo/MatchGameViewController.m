//
//  ViewController.m
//  Matchismo
//
//  Created by stan on 22/07/15.
//  Copyright © 2015 GLoSS. All rights reserved.
//

#import "MatchGameViewController.h"
#import "PlayingCardDeck.h"

@interface MatchGameViewController ()

@end

@implementation MatchGameViewController

- (PlayingCardDeck *)deck {
    return [[PlayingCardDeck alloc] init];
}

- (void)updateUIButton:(UIButton *)cardButton withCard:(Card *)card {
    UIImage *cardBackImage = [UIImage imageNamed:@"cardback.jpg"];
    cardButton.alpha = card.isUnplayable ? 0.3 : 1.0;
    if (!cardButton.isSelected) {
        [cardButton setImage:cardBackImage forState:UIControlStateNormal];
    } else {
        [cardButton setImage:nil forState:UIControlStateNormal];
    }
    cardButton.imageEdgeInsets = UIEdgeInsetsMake(5, 5, 5, 5);
}

@end
