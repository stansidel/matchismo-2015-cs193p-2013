//
//  SetCard.m
//  Matchismo
//
//  Created by Stanislav Sidelnikov on 27/07/15.
//  Copyright © 2015 GLoSS. All rights reserved.
//

#import "SetCard.h"

@implementation SetCard

- (NSInteger)match:(NSArray *)otherCards
{
    NSInteger score = 0;
    
    NSMutableArray *allCards = [[NSMutableArray alloc] initWithArray:otherCards];
    [allCards addObject:self];
    
    NSSet *uniqueColors = [NSSet setWithArray:[allCards valueForKey:@"color"]];
    NSSet *uniqueSymbols = [NSSet setWithArray:[allCards valueForKey:@"symbol"]];
    NSSet *uniqueShadings = [NSSet setWithArray:[allCards valueForKey:@"shading"]];
    NSSet *uniqueNumbers = [NSSet setWithArray:[allCards valueForKey:@"number"]];
    
    if (!(uniqueColors.count == 2 || uniqueSymbols.count == 2
        || uniqueShadings.count == 2 || uniqueNumbers.count == 2)) {
        score = 1;
    }
    
    return score;
}

- (NSString *)contents {
    NSString *contentString = @"";
    for (int i = 0; i < self.number; i++) {
        contentString = [contentString stringByAppendingString:self.symbol];
    }
    return contentString;
}

+ (NSArray *) validSymbols {
    return @[@"●", @"◼︎", @"▲"];
}

+ (NSArray *) validColors {
    return @[@"Green", @"Red", @"Purple"];
}

+ (NSArray *) validShadings {
    return @[@"Solid", @"Striped", @"Open"];
}

+ (NSInteger) maxNumber {
    return 3;
}

@end
