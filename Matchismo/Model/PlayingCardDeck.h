//
//  PlayingCardDeck.h
//  Matchismo
//
//  Created by stan on 23/07/15.
//  Copyright © 2015 GLoSS. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "Deck.h"

@interface PlayingCardDeck : Deck

@end
