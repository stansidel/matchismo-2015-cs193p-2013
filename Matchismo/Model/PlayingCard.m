//
//  PlayingCard.m
//  Matchismo
//
//  Created by stan on 23/07/15.
//  Copyright © 2015 GLoSS. All rights reserved.
//

#import "PlayingCard.h"

@implementation PlayingCard

#define DIFFICULTY_BONUS 2;

- (NSInteger)match:(NSArray *)otherCards
{
    NSInteger score = 0;
    
    NSMutableArray *allCards = [[NSMutableArray alloc] initWithArray:otherCards];
    [allCards addObject:self];
    
    NSSet *uniqueRanks = [NSSet setWithArray:[allCards valueForKey:@"rank"]];
    NSSet *uniqueSuits = [NSSet setWithArray:[allCards valueForKey:@"suit"]];
    // All the cards above the number of unique ranks are cards with matching ranks
    NSInteger rankMatches = allCards.count - uniqueRanks.count;
    // All the cards above the number of unique suits are cards with matching suits
    NSInteger suitsMatches = allCards.count - uniqueSuits.count;
    
    if (rankMatches) {
        score = rankMatches * 4 + (allCards.count - 2) * DIFFICULTY_BONUS;
    } else if (suitsMatches) {
        score = suitsMatches * 1 + (allCards.count - 2) * DIFFICULTY_BONUS;
    }
    
    return score;
}

- (NSString *)contents
{
    NSArray *rankStrings = [PlayingCard rankStrings];
    return [rankStrings[self.rank] stringByAppendingString:self.suit];
}

@synthesize suit = _suit;

+ (NSArray *)validSuits
{
    return @[@"♠︎",@"♣︎",@"♥︎",@"♦︎"];
}

-(void)setSuit:(NSString *)suit
{
    if ([[PlayingCard validSuits] containsObject:suit]) {
        _suit = suit;
    }
}

- (NSString *)suit
{
    return _suit ? _suit : @"?";
}

+ (NSArray *)rankStrings
{
    return @[@"?", @"A", @"2", @"3", @"4", @"5", @"6",
             @"7", @"8", @"9", @"10", @"J", @"Q", @"K"];
}

+ (NSUInteger)maxRank
{
    return [self.rankStrings count] - 1;
}

@end
