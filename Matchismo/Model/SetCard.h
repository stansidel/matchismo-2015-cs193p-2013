//
//  SetCard.h
//  Matchismo
//
//  Created by Stanislav Sidelnikov on 27/07/15.
//  Copyright © 2015 GLoSS. All rights reserved.
//

#import "Card.h"

@interface SetCard : Card

@property (strong, nonatomic) NSString *symbol;
@property (nonatomic) NSInteger number;
@property (strong, nonatomic) NSString *shading;
@property (strong, nonatomic) NSString *color;

+ (NSArray *) validSymbols;
+ (NSArray *) validShadings;
+ (NSArray *) validColors;
+ (NSInteger) maxNumber;

@end
