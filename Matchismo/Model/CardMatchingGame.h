//
//  CardMatchingGame.h
//  Matchismo
//
//  Created by stan on 23/07/15.
//  Copyright © 2015 GLoSS. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "Deck.h"

@interface CardMatchingGame : NSObject

- (id)initWithCardCount:(NSUInteger)cardCount usingDeck:(Deck *)deck;

- (void)flipCardAtIndex:(NSUInteger)index;

- (Card *)cardAtIndex:(NSUInteger)index;

- (NSDictionary *)lastFlip;

- (BOOL)mayChangeMode;

@property (nonatomic, readonly) int score;
@property (nonatomic, readwrite) NSUInteger numCardsMatch;

@end
