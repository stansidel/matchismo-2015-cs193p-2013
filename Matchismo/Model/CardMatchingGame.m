//
//  CardMatchingGame.m
//  Matchismo
//
//  Created by stan on 23/07/15.
//  Copyright © 2015 GLoSS. All rights reserved.
//

#import "CardMatchingGame.h"

@interface CardMatchingGame()
@property (strong, nonatomic) NSMutableArray *cards;
@property (nonatomic) int score;
@property (strong, nonatomic) NSMutableArray *flipsLog;
@property (nonatomic) BOOL gameStarted;
@end

@implementation CardMatchingGame

- (NSMutableArray *) cards {
    if (!_cards) _cards = [[NSMutableArray alloc] init];
    return _cards;
}

- (NSMutableArray *)flipsLog {
    if (!_flipsLog) _flipsLog = [[NSMutableArray alloc] init];
    return _flipsLog;
}

- (void)logCardFlip:(Card *)card {
    [self.flipsLog addObject:@{@"card": card}];
}

- (void)logCardFlip:(Card *)card withPoints:(int)points andOtherCards:(NSArray *)otherCards {
    [self.flipsLog addObject:@{
                               @"card": card,
                               @"points": @(points),
                               @"flippedCards": [otherCards arrayByAddingObject:card]
                               }
     ];
}

- (NSDictionary *)lastFlip {
    return [self.flipsLog lastObject];
}

- (id)initWithCardCount:(NSUInteger)count usingDeck:(Deck *)deck {
    self = [super init];
    
    if (self) {
        for (int i = 0; i < count; i++) {
            Card *card = [deck drawRandomCard];
            if (!card) {
                self = nil;
            } else {
                self.cards[i] = card;
            }
        }
    }
    
    self.numCardsMatch = 2;
    
    return self;
}

- (Card *)cardAtIndex:(NSUInteger)index {
    return (index < self.cards.count) ? self.cards[index] : nil;
}

#define FLIP_COST 1
#define MISMATCH_PENALTY 2
#define MATCH_BONUS 4

- (void)flipCardAtIndex:(NSUInteger)index {
    self.gameStarted = YES;
    Card *card = [self cardAtIndex:index];
    
    if (!card.isUnplayable) {
        if (!card.isFaceUp) {
            NSMutableArray *selectedCards = [[NSMutableArray alloc] init];
            for (Card *otherCard in self.cards) {
                if (otherCard.isFaceUp && !otherCard.isUnplayable && otherCard) {
                    [selectedCards addObject:otherCard];
                }
            }
            if ([selectedCards count] + 1 == self.numCardsMatch) {
                int matchScore = [card match:selectedCards];
                int points = 0;

                if (matchScore) {
                    for (Card *otherCard in selectedCards) {
                        otherCard.unplayable = YES;
                    }
                    card.unplayable = YES;
                    points = matchScore * MATCH_BONUS;
                } else {
                    for (Card *otherCard in selectedCards) {
                        otherCard.faceUp = NO;
                    }
                    points = -MISMATCH_PENALTY;
                }
                self.score += points;
                [self logCardFlip:card withPoints:points andOtherCards:selectedCards];
            } else {
                [self logCardFlip:card];
            }
            self.score -= FLIP_COST;
        }
        card.faceUp = !card.isFaceUp;
    }
}

- (void)setNumCardsMatch:(NSUInteger)numCardsMatch {
    if ([self mayChangeMode]) {
        if (numCardsMatch < 2) {
            numCardsMatch = 2;
        }
        _numCardsMatch = numCardsMatch;
    }
}

- (BOOL)mayChangeMode{
    return !self.gameStarted;
}

@end
