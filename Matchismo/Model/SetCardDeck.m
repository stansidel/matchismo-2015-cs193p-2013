//
//  SetCardDeck.m
//  Matchismo
//
//  Created by Stanislav Sidelnikov on 27/07/15.
//  Copyright © 2015 GLoSS. All rights reserved.
//

#import "SetCardDeck.h"
#import "SetCard.h"

@implementation SetCardDeck

- (instancetype) init
{
    self = [super init];
    
    if (self) {
        for (NSString *symbol in [SetCard validSymbols]) {
            for (NSString *color in [SetCard validColors]) {
                for (NSString *shading in [SetCard validShadings]) {
                    for (NSInteger n = 1; n <= [SetCard maxNumber]; n++) {
                        SetCard *card = [[SetCard alloc] init];
                        card.symbol = symbol;
                        card.color = color;
                        card.shading = shading;
                        card.number = n;
                        [self addCard:card];
                    }
                }
            }
        }
    }
    
    return self;
}

@end
