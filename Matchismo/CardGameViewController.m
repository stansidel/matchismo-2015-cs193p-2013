//
//  CardGameViewController.m
//  Matchismo
//
//  Created by stan on 27/07/15.
//  Copyright © 2015 GLoSS. All rights reserved.
//

#import "CardGameViewController.h"
#import "Deck.h"
#import "Card.h"
#import "CardMatchingGame.h"

@interface CardGameViewController()

@property (weak, nonatomic) IBOutlet UILabel *flipsLabel;
@property (nonatomic) int flipCount;
@property (strong, nonatomic) IBOutletCollection(UIButton) NSArray *cardButtons;
@property (weak, nonatomic) IBOutlet UILabel *scoreLabel;
@property (weak, nonatomic) IBOutlet UILabel *resultInfoLabel;
@property (strong, nonatomic) CardMatchingGame *game;

@end

@implementation CardGameViewController

- (void)setFlipCount:(int)flipCount {
    _flipCount = flipCount;
    self.flipsLabel.text = [NSString stringWithFormat:@"Flips: %d", self.flipCount];
    NSLog(@"flips updated to %d", self.flipCount);
}

- (IBAction)flipCard:(UIButton *)sender {
    [self.game flipCardAtIndex:[self.cardButtons indexOfObject:sender]];
    self.flipCount++;
    [self updateUI];
}

- (CardMatchingGame *)game {
    if (!_game) {
        _game = [[CardMatchingGame alloc] initWithCardCount:self.cardButtons.count
                                                  usingDeck:[self deck]];
        _game.numCardsMatch = [self gameMode];
    }
    return _game;
}

- (Deck *)deck {
    return nil;
}

- (void)setCardButtons:(NSArray *)cardButtons {
    _cardButtons = cardButtons;
    [self updateUI];
}

- (void)updateUI {
    for (UIButton *cardButton in self.cardButtons) {
        Card *card = [self.game cardAtIndex:[self.cardButtons indexOfObject:cardButton]];
        
        [cardButton setTitle:card.contents forState:UIControlStateSelected];
        [cardButton setTitle:card.contents forState:UIControlStateSelected|UIControlStateDisabled];
        cardButton.selected = card.isFaceUp;
        cardButton.enabled = !card.isUnplayable;
        
        [self updateUIButton:cardButton withCard:card];
    }
    self.scoreLabel.text = [NSString stringWithFormat:@"Score: %d", self.game.score];
    [self updateInfoLabel:self.resultInfoLabel forGame:self.game];
}

- (void)updateInfoLabel:(UILabel *)label forGame:(CardMatchingGame *)game {
    NSDictionary *lastFlip = [game lastFlip];
    NSString *infoLabelText = @"Turn a card";
    NSArray *cardsInString = nil;
    if (lastFlip) {
        if ([lastFlip objectForKey:@"points"] && [lastFlip objectForKey:@"flippedCards"]) {
            NSArray *cardContents = [lastFlip[@"flippedCards"] valueForKey:@"contents"];
            cardsInString = lastFlip[@"flippedCards"];
            int points = [lastFlip[@"points"] integerValue];
            if (points > 0) {
                infoLabelText = [NSString stringWithFormat:@"Matched %@ for %d points",
                                 [cardContents componentsJoinedByString:@" & "],
                                 points
                                 ];
            } else {
                infoLabelText = [NSString stringWithFormat:@"%@ don't match! %d point penalty!",
                                 [cardContents componentsJoinedByString:@" & "],
                                 -points
                                 ];
            }
        } else {
            Card *flippedCard = lastFlip[@"card"];
            infoLabelText = [NSString stringWithFormat:@"Flipped up %@",
                             flippedCard.contents];
            cardsInString = @[flippedCard];
        }
    }
    
    [self setInfoText:infoLabelText forLabel:label withCards:cardsInString];
}

- (void)setInfoText:(NSString *)text forLabel:(UILabel *)label withCards:(NSArray *)cards {
    label.text = text;
}

/**
 * Override this method to provide additional visualization
 */
- (void)updateUIButton:(UIButton *)cardButton withCard:(Card *)card {

}

- (void)startOver {
    _game = nil;
    self.flipCount = 0;
    [self updateUI];
}

- (void)alertView:(UIAlertView *)alertView clickedButtonAtIndex:(NSInteger)buttonIndex{
    if (buttonIndex == 0){
        [self startOver];
    }
}

- (IBAction)reDealGame:(UIButton *)sender {
    UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Deal"
                                                    message:@"Are you sure you want to re-deal the game?"
                                                   delegate:self
                                          cancelButtonTitle:@"Yes"
                                          otherButtonTitles:@"No",
                          nil];
    [alert show];
}

- (NSUInteger)gameMode {
    return 2;
}

@end
