//
//  main.m
//  Matchismo
//
//  Created by stan on 22/07/15.
//  Copyright © 2015 GLoSS. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "AppDelegate.h"

int main(int argc, char * argv[]) {
    @autoreleasepool {
        return UIApplicationMain(argc, argv, nil, NSStringFromClass([AppDelegate class]));
    }
}
