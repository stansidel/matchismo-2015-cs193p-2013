//
//  SetGameViewController.m
//  Matchismo
//
//  Created by Stanislav Sidelnikov on 27/07/15.
//  Copyright © 2015 GLoSS. All rights reserved.
//

#import "SetGameViewController.h"
#import "SetCardDeck.h"
#import "SetCard.h"
#import "CardMatchingGame.h"

@interface SetGameViewController ()

@end

@implementation SetGameViewController

- (SetCardDeck *)deck {
    return [[SetCardDeck alloc] init];
}

- (NSUInteger)gameMode {
    return 3;
}

- (void)updateUIButton:(UIButton *)button withCard:(SetCard *)card {
    NSAttributedString *attributedTitle = [self getAttributedStringForCard:card];
    [button setAttributedTitle:attributedTitle forState:UIControlStateNormal];
    [button setAttributedTitle:attributedTitle forState:UIControlStateSelected];
    if (card.isFaceUp) {
        [button setBackgroundColor:[[UIColor grayColor] colorWithAlphaComponent:0.3]];
    } else {
        [button setBackgroundColor:nil];
    }
    button.hidden = card.isUnplayable;
}

- (NSAttributedString *)getAttributedStringForCard:(SetCard *)card {
    NSDictionary *stringAttributes = [self titleAttributesForCard:card];
    
    return [[NSAttributedString alloc] initWithString:[card contents]
                                           attributes:stringAttributes];
}

- (NSDictionary *)titleAttributesForCard:(SetCard *)card {
    UIColor *cardColor = [self getColorByName:card.color];
    UIColor *fillColor = [cardColor colorWithAlphaComponent:[self getAlphaForShading:card.shading]];
    return @{
             NSForegroundColorAttributeName: fillColor,
             NSStrokeWidthAttributeName: @-5,
             NSStrokeColorAttributeName: cardColor,
             };
}

- (CGFloat)getAlphaForShading:(NSString *)shadingName {
    CGFloat alpha = 1;
    if ([shadingName isEqualToString:@"Striped"]) {
        alpha = 0.3;
    } else if ([shadingName isEqualToString:@"Open"]) {
        alpha = 0;
    }
    return alpha;
}

- (UIColor *)getColorByName:(NSString *)colorName {
    UIColor *color = nil;
    if ([colorName isEqualToString:@"Green"]) {
        color = [UIColor greenColor];
    } else if ([colorName isEqualToString:@"Red"]) {
        color = [UIColor redColor];
    } else if ([colorName isEqualToString:@"Purple"]) {
        color = [UIColor purpleColor];
    }
    return color;
}


- (void)setInfoText:(NSString *)text forLabel:(UILabel *)label withCards:(NSArray *)cards {
    if (cards) {
        label.attributedText = [self getAttributedStringVersion:text
                                                       forCards:cards];
    } else {
        label.text = text;
    }
}

- (NSMutableAttributedString *)getAttributedStringVersion:(NSString *)string forCards:(NSArray *)cards {
    NSMutableAttributedString *resultString = [[NSMutableAttributedString alloc] initWithString:string];
    NSRange remainingRange;
    remainingRange.location = 0;
    remainingRange.length = string.length;
    for (Card *card in cards) {
        if ([card isKindOfClass:[SetCard class]]) {
            SetCard *setCard = (SetCard*) card;
            NSRange range = [string rangeOfString:setCard.contents
                                          options:NSLiteralSearch
                                            range:remainingRange];
            
            if (range.location != NSNotFound) {
                NSDictionary *attributes = [self titleAttributesForCard:setCard];
                [resultString setAttributes:attributes range:range];
                remainingRange.location = range.location + range.length;
                remainingRange.length = string.length - remainingRange.location;
            }
        }
    }
    return resultString;
}


@end
